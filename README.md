# PDFMerge
A really simple and open-source tool to merge PDFs.

Downloads (v1.0)
- [Setup](https://github.com/ZetaPhase/PDFMerge/releases/download/v1.0/PDFMerge.msi)
- [Archive](https://github.com/ZetaPhase/PDFMerge/releases/download/v1.0/PDFMerge-compressed.7z)

This was created for two reasons:
- To test out the PDFSharp library
- To make it easy for people to merge PDFs without falling into the tyranny of Adobe

(c) ZetaPhase Technologies, 2016.
Licensed under the MIT License.
